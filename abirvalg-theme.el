(deftheme abirvalg
  "Created 2013-12-11.")

(custom-theme-set-variables
 'abirvalg
 '(ecb-create-layout-frame-width 70)
 '(ecb-display-image-icons-for-semantic-tags nil)
 '(ecb-layout-window-sizes (quote (quote (("left8" (ecb-directories-buffer-name 0.22627737226277372 . 0.28888888888888886) (ecb-sources-buffer-name 0.22627737226277372 . 0.24444444444444444) (ecb-methods-buffer-name 0.22627737226277372 . 0.28888888888888886) (ecb-history-buffer-name 0.22627737226277372 . 0.16666666666666666))))))
 '(ecb-minor-mode-text "")
 '(ecb-options-version "2.40")
 '(ecb-tip-of-the-day nil)
 '(ecb-tree-buffer-style (quote (quote ascii-guides)))
 '(line-spacing nil)
 '(sh-indentation 2)
 '(tty-menu-open-use-tmm nil))

(custom-theme-set-faces
 'abirvalg

 '(default ((t (:family "Monospace" :foundry "unknown" :width normal :height 83 :weight normal :slant normal :underline nil :overline nil :strike-through nil :box nil :inverse-video nil :foreground "black" :background "white" :stipple nil :inherit nil))))
 '(font-lock-comment-face ((t (:foreground "gray"))))
 '(minibuffer-prompt ((t (:foreground "medium blue" :weight bold))))
 '(mode-line ((t (:background "grey75" :foreground "black" :box (:line-width 1 :color "grey75" :style released-button)))))
 '(mode-line-inactive ((t (:inherit mode-line :background "grey90" :foreground "grey20" :box (:line-width 1 :color "grey75" :style released-button) :weight light))))
 '(region ((t (:background "light steel blue"))))
 ;; Gnus
 '(gnus-header-content ((t (:foreground "indianred4"))))
 '(gnus-summary-high-ancient ((t (:foreground "RoyalBlue" :weight bold))) t)
 '(gnus-summary-high-unread ((t (:weight bold))))
 '(gnus-summary-low-read ((t (:foreground "dark gray"))) t)
 '(gnus-summary-low-unread ((t nil))))

(provide-theme 'abirvalg)
