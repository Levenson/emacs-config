;; -*- mode: emacs-lisp -*-

(require 'cl)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Load CEDET.
;; See cedet/common/cedet.info for configuration details.
;; IMPORTANT: Tou must place this *before* any CEDET component (including
;; EIEIO) gets activated by another package (Gnus, auth-source, ...).
(load-file "/home/abralek/emacs-config/site/cedet/cedet-devel-load.el")

;; Add further minor-modes to be enabled by semantic-mode.
;; See doc-string of `semantic-default-submodes' for other things
;; you can use here.

(setq semantic-default-submodes '(global-semantic-idle-scheduler-mode
				  global-semanticdb-minor-mode
				  global-semantic-idle-summary-mode
				  global-semantic-idle-completions-mode
				  ;; global-semantic-decoration-mode
				  ;; global-semantic-highlight-func-mode
				  global-semantic-mru-bookmark-mode
				  ;; global-semantic-show-unmatched-syntax-mode
				  ;; global-semantic-show-parser-state-mode
				  ;; global-semantic-highlight-edits-mode
				  ))

;; (add-to-list 'load-path (expand-file-name "~/Documents/elisp/jdibug-0.2"))
(setq semantic-load-turn-everything-on t)

;; Enable Semantic
(semantic-mode 1)

;; Enable EDE (Project Management) features
(global-ede-mode 1)

(require 'semantic/senator)
(require 'semantic)
(require 'semantic/ia)
(require 'semantic/wisent)
(require 'semantic/wisent/java-tags)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq server-use-tcp t)

;;; DEBUG
(setq debug-on-error t)
(setq byte-compile-debug t)

(setq debug-on-error nil)
(setq byte-compile-debug nil)

(set-fringe-mode 0)

(when (fboundp 'menu-bar-mode)
  (menu-bar-mode -1))

(when (fboundp 'tool-bar-mode)
 (tool-bar-mode -1))

(when (fboundp 'scroll-bar-mode)
  (set-scroll-bar-mode nil))

(when (fboundp 'global-font-lock-mode)
  (global-font-lock-mode t))

;; Enable the command `narrow-to-region' ("C-x n n") which is disabled by
;; default.
(put 'narrow-to-region 'disabled nil)


;;; INITIALIZATION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Directory of the Emacs configuration
(defconst +home+ (file-truename (concat (file-name-as-directory (getenv "HOME"))
					(file-name-as-directory "emacs-config"))))

(defun subdir (pathname &optional root)
  (file-truename (concat (if (stringp root)
			     (file-name-as-directory root)
			   +home+)
			 pathname)))


;; never quit by mistake
(global-set-key (kbd "C-x C-c")
		(lambda () (interactive)
		  (cond ((y-or-n-p "Quit editor? ")
			 (save-buffers-kill-emacs)))))

;; never switch to overwrite mode, not even accidentally
(global-set-key (kbd "<insertchar>")
		(lambda () (interactive)
		  (message "Sorry, overwrite mode has been disabled forever.")))

(global-set-key (kbd "M-SPC") 'just-one-space-in-region)

;; cancel case-fold-search for one search
(global-set-key (kbd "<C-M-s>")
		(lambda () (interactive)
		  (let ((case-fold-search nil))
		    (call-interactively 'isearch-forward-regexp))))

(global-set-key (kbd "<C-M-r>")
		(lambda () (interactive)
		  (let ((case-fold-search nil))
		    (call-interactively 'isearch-backward-regexp))))
;;; CASK PACKAGES INIT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'cask "~/.cask/cask.el")

(cask-initialize +home+)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ace-jump

(autoload 'ace-jump-mode-pop-mark "ace-jump-mode"
  "Ace jump back:-)" t)

(eval-after-load "ace-jump-mode"
  '(ace-jump-mode-enable-mark-sync))

(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)
(define-key global-map (kbd "C-x SPC") 'ace-jump-mode-pop-mark)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; browser-url

(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "conkeror")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; buffer-move

(require 'buffer-move)

(global-set-key (kbd "<C-M-up>")     'buf-move-up)
(global-set-key (kbd "<C-M-down>")   'buf-move-down)
(global-set-key (kbd "<C-M-left>")   'buf-move-left)
(global-set-key (kbd "<C-M-right>")  'buf-move-right)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; color-theme

(add-to-list 'custom-theme-load-path +home+)

(unless (null (window-system))
  (load-theme 'abirvalg t))

(load-theme 'zenburn t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(mode-line ((t :box nil)))
 '(mode-line-highlight ((t :box nil)))
 '(mode-line-inactive ((t :box nil))))

(setq line-spacing nil)
(setq inhibit-startup-screen t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ediff

(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; elpy

(elpy-enable)

;; (setq python-shell-interpreter "/usr/bin/python3.4"
;;       python-shell-interpreter-args "-m jedi repl")

(setq python-shell-interpreter "/usr/local/bin/ipython3"
      python-shell-interpreter-args "")

(setq elpy-rpc-python-command "python3.4"
      elpy-eldoc-show-current-function nil
      elpy-rpc-backend "jedi")

(defvar python-extention-list (list "py"))

(defun semanticdb-rescan-directory (pathname)
  (dolist (file (cddr (directory-files pathname t)))
    (if (file-directory-p file)
        (semanticdb-rescan-directory file)
      (when (member (file-name-extension file) python-extention-list)
        (message "Parsing %s file." file)
        (ignore-errors
            (semanticdb-file-table-object file))))))

(defun semantic-python-rescan-includes ()
  (interactive)
  (dolist (includes (semantic-python-get-system-include-path))
    (message "Parsing %s" includes)
    (semanticdb-rescan-directory includes)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; erc

(require 'erc)

(eval-after-load "erc"
  '(let ((erc-settings (subdir ".ercrc.el.gpg")))
     (when (file-exists-p erc-settings)
       (load-library erc-settings))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; eshell

(require 'eshell)
(require 'em-smart)

(setq eshell-where-to-jump 'begin
      eshell-review-quick-command nil
      eshell-smart-space-goes-to-end t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; files

(setq make-backup-files nil
      version-control t
      kept-new-versions 5
      kept-old-versions 5
      dired-kept-versions 1
      auto-save-interval 512
      auto-save-timeout 30)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; flymake

(require 'flymake)

(defun flymake-pylint-init ()
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
                     'flymake-create-temp-inplace))
         (local-file (file-relative-name
                      temp-file
                      (file-name-directory buffer-file-name))))
    (list "epylint" (list local-file))))

(add-to-list 'flymake-allowed-file-name-masks
             '("\\.py\\'" flymake-pylint-init))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; jedi

;; (add-hook 'ein:connect-mode-hook 'ein:jedi-setup)
;; (add-hook 'python-mode-hook 'jedi:setup)

;; (setq jedi:complete-on-dot t
;;       jedi:complete-on-dot t
;;       jedi:install-imenu t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; lisp

(setq parens-require-spaces nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; linum-mode

(add-hook 'linum-before-numbering-hook
	  (lambda()
	    (setq-local linum-format
			(let ((w (length (number-to-string (count-lines (point-min) (point-max))))))
			  (concat " %" (number-to-string w) "d|")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; guide-key

(setq guide-key/guide-key-sequence '("C-x"))
(setq guide-key/recursive-key-sequence-flag t)
(guide-key-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; iedit
(require 'iedit)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; info-look

(require 'info-look)

(add-to-list 'Info-additional-directory-list "~/.emacs.d/info")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; irfc
(require 'irfc)

(setq irfc-directory "~/.emacs.d/irfc")

(setq irfc-assoc-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; indent

;; Set standard indent to 2 rather that 4
(setq standard-indent 2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; magit

(require 'magit)

(eval-after-load "magit"
  '(global-set-key (kbd "C-c m") 'magit-status))

(setq magit-last-seen-setup-instructions "1.4.0")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mouse.el

(setq mouse-yank-at-point t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mu4e
(add-to-list 'load-path (subdir "site/mu/mu4e"))

(require 'mu4e)
(require 'mu4e-contrib)

(setq mail-user-agent 'mu4e-user-agent)

(let ((mu4e-settings (subdir "mu4e.el")))
  (when (file-exists-p mu4e-settings)
    (load-library mu4e-settings)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; paredit

(require 'paredit)

(eval-after-load "paredit"
  '(progn
     (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
     (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
     (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
     (add-hook 'slime-repl-mode-hook       #'enable-paredit-mode)
     (add-hook 'scheme-mode-hook           #'enable-paredit-mode)
     (add-hook 'clojure-mode-hook          #'enable-paredit-mode)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; pydoc-info

(require 'pydoc-info)

(info-lookup-add-help
 :mode 'python-mode
 :parse-rule 'pydoc-info-python-symbol-at-point
 :doc-spec
 '(("(python)Index" pydoc-info-lookup-transform-entry)
   ("(sphinx)Index" pydoc-info-lookup-transform-entry)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; rfn-eshadow

(setq file-name-shadow-properties
      file-name-shadow-tty-properties)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; savehist

(setq savehist-file "~/.emacs.d/history")
(savehist-mode 1)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; simple

(setq line-number-mode t)
(setq column-number-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; slime

(setq inferior-lisp-program "/usr/local/bin/sbcl")

;; (require 'slime)
(require 'slime-autoloads)

(setq slime-contribs '(slime-repl slime-asdf
				  slime-fuzzy
				  slime-fancy
				  slime-fontifying-fu
				  slime-hyperdoc
				  slime-autodoc
				  slime-xref-browser
				  slime-references))

(setf slime-complete-symbol-function 'slime-fuzzy-complete-symbol)

(setq slime-net-coding-system 'utf-8-unix)
;;
(setq common-lisp-hyperspec-root
      (concat "file://" (subdir "docs/HyperSpec/")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; smart-mode-line

(setq sml/no-confirm-load-theme t)

(setq sml/theme 'powerline
      sml/shorten-modes nil)

(sml/setup)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; smtpmail

(require 'smtpmail)

;; Thanks for Richard Riley
(defun set-smtp (mech server port user password)
  "Set related SMTP variables for supplied parameters."
  (setq smtpmail-smtp-server server
	smtpmail-smtp-service port
	smtpmail-auth-supported (list mech)
	smtpmail-starttls-credentials nil)
  (message "Setting SMTP server to `%s:%s' for user `%s'."
	   server port user))

(defun set-smtp-ssl (server port user password &optional key cert)
  "Set related SMTP and SSL variables for supplied parameters."
  (setq starttls-use-gnutls t
	starttls-gnutls-program "gnutls-cli"
	;; TODO: Add this to the server parameters
	;; starttls-extra-arguments nil
	smtpmail-smtp-server server
	smtpmail-smtp-service port
	smtpmail-stream-type 'ssl
	smtpmail-starttls-credentials (list (list server port key cert)))
  (message
   "Setting SMTP server to `%s:%s' for user `%s'. (SSL enabled.)"
   server port user))

(defun change-smtp ()
  "Change the SMTP server according to the current from line."
  (interactive)
  (save-excursion
    (loop with from = (save-restriction
			(message-narrow-to-headers)
			(message-fetch-field "from"))
	  for (auth-mech address . auth-spec) in smtp-accounts
	  when (string-match address from)
	  do (cond
	      ((memq auth-mech '(cram-md5 plain login))
	       (return (apply 'set-smtp (cons auth-mech auth-spec))))
	      ((eql auth-mech 'ssl)
	       (return (apply 'set-smtp-ssl auth-spec)))
	      (t (error "Unrecognized SMTP auth. mechanism: `%s'." auth-mech)))
	  finally (error "Cannot infer SMTP information."))))

(setq message-send-mail-function 'smtpmail-send-it)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; uniquify

(require 'uniquify)

;; (setq uniquify-buffer-name-style t)
(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)
(setq uniquify-ignore-buffers-re "^\\*")
(setq post-forward-angle-brackets 'post-forward-angle-brackets)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; helm

(require 'helm)
(require 'helm-config)
(require 'helm-recoll)

;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.

(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)

(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-su-or-sudo "su")

(setq helm-semantic-fuzzy-match t
      helm-imenu-fuzzy-match    t)

(setq helm-locate-fuzzy-match t)

(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-c h g") 'helm-google-suggest)

(helm-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; multiple-cursors

(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; vc

(setq vc-follow-symlinks t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; windmove.el

(windmove-default-keybindings 'shift)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; xclip

(require 'xclip)

(xclip-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(let ((local-settings (subdir "local-settings.el")))
  (when (file-exists-p local-settings)
    (load-library local-settings)))


(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

(defun just-one-space-in-region (beg end)
  "replace all whitespace in the region with single spaces"
  (interactive "r")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (while (re-search-forward "\\s-+" nil t)
	(replace-match " ")))))

(global-set-key (kbd  "C-x t") 'toggle-window-split)

(global-set-key (kbd "C-M-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-M-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-M-<down>") 'enlarge-window )
(global-set-key (kbd "C-M-<up>") 'shrink-window)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hooks
(add-hook 'emacs-startup-hook
          (lambda ()
            (when (boundp 'server-name)
              (with-temp-file (expand-file-name (format "~/.emacs.d/%s.pid" server-name))
                (insert (format "%s\n" (number-to-string (emacs-pid))))))))

(add-hook 'kill-emacs-hook
          (lambda ()
            (when (boundp 'server-name)
              (let ((pidfile (expand-file-name (format "~/.emacs.d/%s.pid" server-name))))
                (when (file-exists-p pidfile)
                  (delete-file pidfile))))))

(add-hook 'before-save-hook 'whitespace-cleanup)

