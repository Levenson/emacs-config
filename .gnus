;; -*- mode: emacs-lisp;  -*-

;; additional packages
(require 'nnir)
(require 'starttls)
(require 'smtpmail)
(require 'spam)

(spam-initialize)

(setq spam-use-bogofilter t)
(setq gnus-spam-process-destinations '(("nnml:.*" "nnml:junk")))
(setq spam-split-group "junk")

;; bbdb
(add-to-list 'load-path (subdir "site/bbdb/lisp"))
(require 'bbdb-loaddefs)

(bbdb-initialize 'gnus 'message)

(setq bbdb-layout 'multi-line
      bbdb-pop-up-window-size 2
      bbdb-mua-pop-up-window-size 2
      bbdb/gnus-update-records-p nil)

(bbdb-mua-auto-update-init 'gnus 'message)

(load-library (subdir "mail-sources.el.gpg"))

(setq gnus-verbose 10)
(setq gnus-verbose-backends 10)
(setq garbage-collection-messages nil)

(setq gnus-show-threads nil)
;; This marks mail I send as read.
(setq gnus-gcc-mark-as-read t)

(setq gnus-read-newsrc-file nil)
(setq gnus-save-newsrc-file nil)

(setq mm-text-html-renderer 'shr)
(setq mm-inline-text-html-with-images t)
(setq mm-inline-text-html-with-w3m-keymap nil)

;; Disable X-face
(setq gnus-article-x-face-too-ugly ".")

(setq sieve-manage-default-port 4190)

(setq gnus-large-newsgroup 10000)
(setq gnus-large-ephemeral-newsgroup 20000)
(setq gnus-newsgroup-maximum-articles 20000)

;;if retrieving from spool, delete temp file after 1 day(s)
(setq mail-source-delete-incoming 1) ;; change 1 to t to immediately delete or any number
(setq mail-source-delete-old-incoming-confirm nil)

(setq gnus-treat-strip-multiple-blank-lines t)
(setq gnus-treat-strip-leading-blank-lines t)
(setq gnus-treat-strip-trailing-blank-lines t)
;; Make sure Gnus doesn't display smiley graphics.
(setq gnus-treat-display-smileys nil)

(setq lotus-wash-ask-p nil)

(defun lotus-article-wash-disclosure ()
  (dolist (item disclosures-list)
    (save-excursion
      (when lotus-wash-ask-p
        (message "Looking for %S" item))
      (while (re-search-forward item nil t)
        (let ((end (point)))
          (push-mark)
          (activate-mark)
          (backward-char (length (match-string 0)))
          (if lotus-wash-ask-p
              (when (y-or-n-p "Delete region? ")
                (message "Removing %S" (buffer-substring (point) end))
                (delete-region (point) end))
            (delete-region (point) end)))))))

(add-hook 'gnus-part-display-hook 'lotus-article-wash-disclosure)

;; Function for extracting address components from a From header. The
;; `mail-extract-address-components' works much better then
;; `gnus-extract-address-components'
(setq gnus-extract-address-components
      'mail-extract-address-components)

;; The primary sort function is the last in the list.
(setq gnus-article-sort-functions
      '(gnus-article-sort-by-number (not gnus-article-sort-by-date))
      gnus-thread-sort-functions
      '(gnus-thread-sort-by-number (not gnus-thread-sort-by-date)))

(setq message-citation-line-function 'message-insert-formatted-citation-line)
(setq message-citation-line-format "On %a, %R %Z, %b %d %Y, %f wrote:\n")

(setq message-make-forward-subject-function
      'message-forward-subject-fwd)

(setq gnus-group-line-format "%M%S%p%P%8y:%B%(%G%)\n"
      gnus-summary-line-format "%U%R%z| %(%&user-date; | %5k | %-40,40F | %B%s%)\n"
      gnus-user-date-format-alist '((t . "%Y-%m-%d %H:%M"))
      gnus-summary-thread-gathering-function 'gnus-gather-threads-by-references
      gnus-sum-thread-tree-false-root ""
      gnus-sum-thread-tree-indent " "
      gnus-sum-thread-tree-leaf-with-other "├► "
      gnus-sum-thread-tree-root ""
      gnus-sum-thread-tree-single-leaf "╰► "
      gnus-sum-thread-tree-vertical "│")

;; (setq smtpmail-debug-info t
;;       smtpmail-debug-verb t)

;; Thanks for Sébastien Vauban
(eval-after-load "mm-uu"
  '(progn
     ;; correctly fontify Org-mode attachments
     (add-to-list 'mailcap-mime-extensions '(".org" . "text/org"))
     (add-to-list 'mm-automatic-display "text/org")

     (add-to-list 'mm-inline-media-tests
                  '("text/org" my/display-org-inline
                    (lambda (el) t)))

     (defun my/display-org-inline (handle)
       (condition-case nil
           (mm-display-inline-fontify handle 'org-mode)
         (error
          (insert
           (with-temp-buffer (mm-insert-part handle) (buffer-string))
           "\n"))))

     ;; fontify code blocks in the text of messages
     (defun my/mm-org-src-code-block-extract ()
       (mm-make-handle (mm-uu-copy-to-buffer start-point end-point)
                       '("text/org")))

     (add-to-list 'mm-uu-type-alist
                  '(org-src-code-block
                    "^[ \t]*#\\+begin_"
                    "^[ \t]*#\\+end_"
                    my/mm-org-src-code-block-extract
                    nil))

     (add-to-list 'mm-uu-type-alist
                  '(org-meta-line
                    "^[ \t]*#\\+[[:alpha:]]+: "
                    "$"
                    my/mm-org-src-code-block-extract
                    nil))

     (mm-uu-configure)))

;; Hooks
(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
(add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)

(add-hook 'message-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c M-o") 'htmlize-gnus-color-plaintext)))
