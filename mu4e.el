
(setq user-full-name "Levenson"
      user-mail-address "Levenson@mmer.org")

(setq smtp-accounts
      '((ssl "levenson@mmer.org" "mail.mmer.org" "smtps" nil nil)
        (ssl "levensn@gmail.com" "smtp.gmail.com" "smtps" nil nil)))

;; Accounts
(setq mu4e-user-mail-address-list
      (quote ("levenson@mmer.org" "levensn@gmail.com")))

(setq mu4e-mu-binary "/usr/local/bin/mu")

;; Kill buffer after sending
(setq message-kill-buffer-on-exit t)
;; Just quit when I say
(setq mu4e-confirm-quit nil)
;; Don't prompt for applying of marks, just apply
(setq mu4e-headers-leave-behavior 'apply)

(setq mu4e-view-prefer-html t
      mu4e-html2text-command "w3m -I utf8 -O utf8 -T text/html -s -graph")

(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

(setq mu4e-view-show-images t)
(setq mu4e-image-max-width 800)

;; Shortcuts
(setq mu4e-maildir-shortcuts
      '(("/inbox"	. ?i)
	("/drafts"	. ?d)
	("/sent"	. ?s)
	("/trash"	. ?t)))

;; Headers
(setq mu4e-headers-include-related nil
      mu4e-headers-skip-duplicates t
      mu4e-headers-visible-lines 15)

(setq mu4e-headers-fields
      (quote ((:human-date . 12)
	      (:flags . 6)
	      (:mailing-list . 10)
	      (:from-or-to . 22)
	      (:subject))))

;; Compose
;; (setq mu4e-compose-complete-only-personal t
;;       mu4e-compose-signature-auto-include nil
;;       mu4e-compose-signature ""
;;       message-citation-line-format "On %a, %b %d %Y, %N wrote:"
;;       message-citation-line-function 'message-insert-formatted-citation-line)

(add-hook 'message-mode-hook (lambda () (flyspell-mode 1)))

;; Reading
(setq mu4e-attachment-dir (expand-file-name "~/downloads"))

;; Use imagemagick, if available
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; Receiving mail
(setq mu4e-get-mail-command "offlineimap"
      mu4e-hide-index-messages t
      mu4e-update-interval 420)

;; Sending mail
(setq mu4e-sent-messages-behavior 'delete)

(add-to-list 'mu4e-view-actions
	     '("ViewInBrowser" . mu4e-action-view-in-browser) t)

;; (defun my-mu4e-view-in-browser ()
;;   "View the body of the message in a web browser."
;;   (interactive)
;;   (let ((html (mu4e-msg-field (mu4e-message-at-point t) :body-html))
;;         (tmpfile (format "%s/%d.html" temporary-file-directory (random))))
;;     (unless html (error "No html part for this message"))
;;     (with-temp-file tmpfile
;;       (insert
;;        "<html>"
;;        "<head><meta http-equiv=\"content-type\""
;;        "content=\"text/html;charset=UTF-8\">"
;;        html))
;;     (browse-url (concat "file://" tmpfile))))

;; (define-key mu4e-view-mode-map (kbd "G") 'my-mu4e-view-in-browser)
